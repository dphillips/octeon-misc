#!/bin/bash -xe

# set LINUX_CACHE to point to a local clone of Linux

HERE=$(realpath "$(dirname "$0")")

mkdir -p /tmp/octeon-linux
cd /tmp/octeon-linux

if [ ! -d linux ]; then
	git clone "$LINUX_CACHE"
fi
cd linux
cp "$HERE/config" .config
cp "$HERE/image.its" image.its

export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm64

make -j45
mkimage -f image.its fitImage-cavium
sudo install -m644 fitImage-cavium /srv/tftp/fitImage-cavium
